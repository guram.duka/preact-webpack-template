//------------------------------------------------------------------------------
import Dexie from 'dexie';
import Deque from 'double-ended-queue';
import setZeroTimeout from './zerotimeout';
import RedBlackTree from './rbt';
import { copy, equal } from './util';
import { newEnum } from './enum';
import root from './root';
//------------------------------------------------------------------------------
const LogOps = newEnum({ PUT: 1, DEL: 2 });
//------------------------------------------------------------------------------
const collator = new Intl.Collator();
//------------------------------------------------------------------------------
function compareByParent(a, b) {
	return a.parent - b.parent;
}
//------------------------------------------------------------------------------
function compareByParentAndName(a, b) {
	let c = a.parent - b.parent;

	if (c === 0) {
		if (Number.isFinite(a) || Number.isFinite(b))
			c = a.name - b.name;
		else
			c = collator.compare(a.name, b.name);
	}

	return c;
}
//------------------------------------------------------------------------------
function transformPath(path) {
	if (Array.isArray(path)) {
		path = Array.from(path);
	}
	else if (path && (path.constructor === String || path instanceof String)) {
		const s = path.split('.');
		path = s.length === 0 ? (path.length === 0 ? [] : [path]) : s;
	}
	else
		path = [];

	for (let i = path.length - 1; i >= 0; i--) {
		const n = +path[i];

		if (!Number.isNaN(n))
			path[i] = n;

		const p = path[i];

		if (p === null || p === undefined ||
			!(
				Number.isInteger(p)
				|| p.constructor === String
				|| p instanceof String
			)
		)
			throw new Error('Invalid path entry');
	}

	if (path.length === 0)
		throw new Error('Invalid path');

	return path;
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class State {
	loader(globals, exclude) {
		const db = this.__db = new Dexie('state');

		// Define schema
		db.version(1).stores({ state: 'id' }); // Primary Key is not auto-incremented (id)

		// Open the database
		//db.open().catch(reject);
		const { state } = db;

		return db.transaction('r', [state]).then(
			tx => state.orderBy('id').toArray()
		).then(
			rows => store.pullState({ rows, globals, exclude })
		);
	}

	pullState(state) {
		this.__actions = new Deque(16);
		this.__topics = new Deque(16);
		this.__subscribers = [];
		this.__subscribersBindings = new Map();

		const unpackState = () => {
			let id = 0;
			const tree = new RedBlackTree(compareByParentAndName);
			const treeMap = new Map();
			let deletions;

			const { rows, globals, exclude } = state;

			if (rows && rows.length !== 0) {
				for (const row of rows) {
					const { parent, name, value } = row;

					const g = globals[name];

					if (parent === 1 && g)
						root[g] = value;

					delete row.value;

					tree.set(row, value);

					let pSet = treeMap.get(parent);

					if (pSet === undefined)
						treeMap.set(parent, pSet = new Set());

					pSet.add(name);

					if (row.id > id)
						id = row.id;
				}

			}

			deletions = exclude;

			return [tree, treeMap, id + 1, deletions];
		};

		[this.__tree, this.__treeMap, this.__nextId, this.__nostore] = unpackState();

		if (this.__nostore) {
			for (const path of this.__nostore)
				this.delete(path, true, 0);
			delete this.__nostore;
		}
	}

	__performNodeActions = () => this.__pushState()
	__pushState(force) {
		const { __actions } = this;

		if (DEVELOPMENT && __actions.isEmpty())
			throw new Error('Store actions must not be empty');

		const actions = [], ops = [];
		// with large updates, allow the interface to be responsive
		const maxOps = 10;
		let action;

		while (!__actions.isEmpty() && (ops.length < maxOps || force)) {
			action = __actions.peek();

			if (action.op !== LogOps.PUT)
				break;

			const { key, value } = action;

			ops.push({ ...key, value });
			__actions.shift();
			actions.push(action);
		}

		if (ops.length === 0 && !__actions.isEmpty()) {
			action = __actions.peek();

			if (action.op !== LogOps.DEL)
				throw new Error('Invalid action operation');

			ops.push(action.key.id);
			__actions.shift();
			actions.push(action);
		}

		const db = this.__db;
		const { state } = db;

		db.transaction('rw', [state]).then(tx =>
			action.op === LogOps.PUT
				? state.bulkPut(ops)
				: state.bulkDelete(ops)
		).then(
			() => actions.length = 0
		).finally(() => {
			__actions.unshift(actions);
			return __actions.isEmpty()
				? delete this.__performingNodeActions
				: setZeroTimeout(this.__performNodeActions);
		});
	}

	__scheduleNodeAction(node, deletion) {
		const entry = {
			op: deletion ? LogOps.DEL : LogOps.PUT,
			key: node.key
		};

		if (!deletion)
			entry.value = copy(node.value);

		this.__actions.push(entry);

		if (!this.__performingNodeActions) {
			setZeroTimeout(this.__performNodeActions);
			this.__performingNodeActions = true;
		}

		return this;
	}

	__createNode(key) {
		this.__tree.set(key);
		const node = this.__tree.__search(key);
		this.__scheduleNodeAction(node);
		this.__nextId++;

		let pSet = this.__treeMap.get(key.parent);

		if (pSet === undefined)
			this.__treeMap.set(key.parent, pSet = new Set());

		pSet.add(key.name);

		return node;
	}

	__getVPath(path, create = true) {
		let key = { id: this.__nextId, parent: 0, name: '' };
		let node = this.__tree.__search(key);

		// only if root node not created
		if (node) {
			key = node.key;
		}
		else if (create) {
			node = this.__createNode(key);
		}
		else
			return undefined;

		for (const k of path) {
			key = { id: this.__nextId, parent: key.id, name: k };
			node = this.__tree.__search(key);

			if (node) {
				key = node.key;
			}
			else if (create) {
				node = this.__createNode(key);
			}
			else
				return undefined;
		}

		return node;
	}

	set(path, value, pubLevels = 1, equ = equal) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		if (equ(node.value, value))
			return this;

		node.value = value;
		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	replace(path, value, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = value;

		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	transform(path, transformator, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = transformator(node.value);

		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	delete(path, deep = true, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path, false);

		if (node) {
			this.__scheduleNodeAction(node, true);
			const { key } = node;

			if (deep) {
				let pSet = this.__treeMap.get(key.id);

				if (pSet)
					for (const n of Array.from(pSet))
						this.delete(path.concat(n), deep, pubLevels);
			}

			const deleted = this.__tree.delete(key);

			if (!deleted)
				throw new Error('Undefined behavior');

			let pSet = this.__treeMap.get(key.parent);

			if (pSet === undefined)
				throw new Error('Undefined behavior');

			pSet.delete(key.name);

			if (pSet.size === 0)
				this.__treeMap.delete(key.parent);

			return this.__pub(path, pubLevels);
		}

		return this;
	}

	undef(path, value, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path, value !== undefined);

		if (node) {
			if (value !== undefined) {
				if (node.value !== value) {
					node.value = value;
					return this.__scheduleNodeAction(node).__pub(path, pubLevels);
				}
			}
			else {
				this.__scheduleNodeAction(node, true);
				const deleted = this.__tree.delete(node.key);

				if (!deleted)
					throw new Error('Undefined behavior');

				return this.__pub(path, pubLevels);
			}
		}

		return this;
	}

	toggle(path, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = !node.value;
		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	inc(path, value = 1, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = ~~node.value + value;
		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	dec(path, value = 1, pubLevels = 1) {
		path = transformPath(path);
		const node = this.__getVPath(path);

		node.value = ~~node.value - value;
		return this.__scheduleNodeAction(node).__pub(path, pubLevels);
	}

	has(path) {
		path = transformPath(path);
		const node = this.__getVPath(path, false);
		return node !== undefined;
	}

	get(path, defaultValue) {
		path = transformPath(path);
		const node = this.__getVPath(path, false);

		if (node !== undefined)
			return node.value;

		return defaultValue;
	}

	__pub(path, levels = 1) {
		let l = levels;

		while (l !== 0) {
			l--;
			this.__topics.push(path.slice(0, path.length - l).join('.'));
		}

		if (!this.__performingPublishing) {
			setZeroTimeout(this.__performPublishing);
			this.__performingPublishing = true;
		}

		return this;
	}

	pub(path, levels = 1) {
		if (levels < 0)
			throw new Error('Invalid value');

		if (levels === 0)
			return this;

		return this.__pub(transformPath(path), levels);
	}

	publish() {
		this.__publishing();
	}

	__performPublishing = () => this.__publishing()
	__publishing() {
		const topics = this.__topics.toArray();
		this.__topics.clear();

		for (const subscriber of this.__subscribers) {
			const object = this.__subscribersBindings.get(subscriber);

			if (object)
				subscriber.call(object, topics);
			else
				subscriber(topics);
		}

		delete this.__performingPublishing;
	}

	subscribe(subscriber, object) {
		if (this.__subscribersBindings.has(subscriber))
			throw new Error('Already subscribed');

		this.__subscribers.push(subscriber);

		if (object)
			this.__subscribersBindings.set(subscriber, object);
	}

	unsubscribe(subscriber) {
		if (!this.__subscribersBindings.has(subscriber))
			throw new Error('Not subscribed');

		this.__subscribers.splice(this.__subscribers.indexOf(subscriber), 1);
		this.__subscribersBindings.delete(subscriber);
	}

	__print(parent = { parent: 1 }, path, s = []) {
		const range = new RedBlackTree.KeyRange(parent);

		for (const [k, v] of this.__tree.entries(range, compareByParent)) {
			const p = (path ? path + '.' : '') + k.name;

			if (v !== undefined)
				s.push([p, v]);

			this.__print({ parent: k.id }, p, s);
		}

		return s;
	}
}
//------------------------------------------------------------------------------
const store = new State();
//------------------------------------------------------------------------------
export default store;
//------------------------------------------------------------------------------
export function loader(...args) {
	return store.loader(...args);
}
//------------------------------------------------------------------------------
