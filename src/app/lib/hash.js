//------------------------------------------------------------------------------
// Generates 64 bit integer hashes.
// Zero collisions for hashing 65 million strings.
// https://github.com/mstdokumaci/string-hash-64
//------------------------------------------------------------------------------
const hash1 = 5381, hash2 = 52711;
//------------------------------------------------------------------------------
export function hash12() {
	return [ hash1, hash2 ];
}
//------------------------------------------------------------------------------
// value type supported only integer or string
//------------------------------------------------------------------------------
export default function hash(value, h12) {
	let h1, h2;

	if (Array.isArray(h12)) {
		[h1, h2] = h12;
	}
	else {
		h1 = hash1;
		h2 = hash2;
	}

	if (Number.isInteger(value)) {
		while (value !== 0) {
			const byte = value & 0xff;
			value >>>= 8;
			h1 = (h1 * 33) ^ byte;
			h2 = (h2 * 33) ^ byte;
		}
	}
	else {
		let i = value.length;

		while (i--) {
			const char = value.charCodeAt(i);
			h1 = (h1 * 33) ^ char;
			h2 = (h2 * 33) ^ char;
		}
	}

	h12[0] = h1;
	h12[1] = h2;

	return (h1 >>> 0) * 4096 + (h2 >>> 0);
}
//------------------------------------------------------------------------------
