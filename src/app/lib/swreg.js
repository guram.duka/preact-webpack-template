//------------------------------------------------------------------------------
// Let's allow our Service Worker to come out and play by registering it.
//------------------------------------------------------------------------------
if ('serviceWorker' in navigator) {
	window.addEventListener('load', () => {
		navigator.serviceWorker.register(PUBLIC_PATH + 'service-worker.js').then(
			registration => console.log('SW registered: ', registration)
		).catch(
			error => console.log('SW registration failed: ', error)
		);
	});
	window.addEventListener('activate',
		activation => console.log('SW activating', activation)
	);
}
//------------------------------------------------------------------------------
