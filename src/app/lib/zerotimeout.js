//------------------------------------------------------------------------------
import Deque from 'double-ended-queue';
import root from './root';
import { prevent } from './util';
//------------------------------------------------------------------------------
const timeouts = new Deque(8);
const messageName = 'zero-timeout-message';
//------------------------------------------------------------------------------
function messageHandler(e) {
	if (e.source === root && e.data === messageName) {
		while (!timeouts.isEmpty())
			try {
				timeouts.shift()(e);
			}
			catch (e) {
				root.console.error(e.stack | e);
				throw e;
			}

		return prevent(e);
	}
}
//------------------------------------------------------------------------------
root.addEventListener && root.addEventListener.constructor === Function
	&& root.addEventListener('message', messageHandler, true);
//------------------------------------------------------------------------------
export default function setZeroTimeout(fn) {
	timeouts.push(fn);
	root.postMessage && window.postMessage(messageName, '*');
}
//------------------------------------------------------------------------------
