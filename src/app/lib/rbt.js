// from https://github.com/aureooms/js-red-black-tree/
// modified by Guram Duka
//------------------------------------------------------------------------------
const RED = 0, BLACK = 1;
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * A black leaf node.
 *
 * @constructor
 * @param {Node} parent - The parent node in the tree.
 * @returns {Leaf}
 */
class Leaf {
	constructor(parent) {
		this.color = BLACK;
		this.parent = parent;
	}

	/**
     * Returns <code>true</code> if the <code>Leaf</code> object is a leaf. This
     * always returns <code>true</code>.
     *
     * @returns {Boolean}
     */
	isLeaf() {
		return true;
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * An internal node. This node can be red or black.
 *
 * @constructor
 * @param {Number} color - The color of the node.
 * @param {Key} key - The key of the node.
 * @param {Value} value - The value of the node.
 * @returns {Node}
 */
class Node {
	constructor(color, key, value) {
		this.color = color;
		this.left = new Leaf(this);
		this.right = new Leaf(this);
		this.parent = null;
		this.key = key;
		this.value = value;
	}

	path() {
		const path = [];

		for (let node = this; node !== null; node = node.parent)
			path.push(node.key);

		path.reverse();

		return path;
	}

	pathName() {
		const path = [];

		for (let node = this; node !== null; node = node.parent)
			path.push(node.key.name);

		path.reverse();

		return path;
	}

	/**
     * Returns <code>true</code> if the <code>Node</code> object is a leaf. This
     * always returns <code>false</code>.
     *
     * @returns {Boolean}
     */
	isLeaf() {
		return false;
	}
}
//------------------------------------------------------------------------------
/**
 * Computes the predecessor of the input node, in the subtree rooted at the
 * input node, when this predecessor is guaranteed to exist.
 *
 * @param {Node} node - The input node.
 * @returns {Node}
 */
function predecessor(node) {
	// assert( !node.left.isLeaf() ) ;
	let pred = node.left;

	while (!pred.right.isLeaf())
		pred = pred.right;

	return pred;
}
//------------------------------------------------------------------------------
/**
 * Walks the tree rooted at <code>A</code> down the only path that satisfies
 * the following property: if at a node <code>C</code> we make a left (resp.
 * right), then <code>B < C</code> (resp. <code>B >= C</code>). Once we hit the
 * end of the path, we can add node <code>B</code> at this position. By the
 * property of the path, the tree rooted at <code>A</code> is still a binary
 * search tree.
 * For our red-black tree, all that is left to do is fix the red-black tree
 * properties in case they have been violated by this insertion. This is fixed
 * by {@link insertCase1}.
 *
 * @param compare - The comparison function to use.
 * @param {Node} A - The root of the tree.
 * @param {Node} B - The node to insert.
 * @returns {Node} B - The node that has been inserted.
 */
function insert(compare, A, key, value, replace) {
	let B, c;

	for (; ;) {
		c = compare(key, A.key);

		if (c < 0) {
			const node = A.left;

			if (node.isLeaf()) {
				A.left = B = new Node(RED, key, value);
				break;
			}

			A = node;
		}
		else if (c > 0) {
			const node = A.right;

			if (node.isLeaf()) {
				A.right = B = new Node(RED, key, value);
				break;
			}

			A = node;
		}
		else {//if (c === 0)
			//if (arguments.length === 4)
			//	throw Error('Can\'t add, key exists');

			if (replace)
				A.value = value;

			return false;
		}
	}

	B.parent = A;
	insertCase2(B);

	return true;
}
//------------------------------------------------------------------------------
/**
 * Computes the grandparent (parent of parent) of the input node.
 *
 * @param {Node} node - The input node.
 * @returns {Node}
 */
function grandparent(node) {
	// assert((node !== null) && (node.parent !== null));
	// We only call this function when node HAS a grandparent
	return node.parent.parent;
}
//------------------------------------------------------------------------------
/**
 * Computes the uncle of the input node when the grandparent is guaranteed to
 * exist.
 *
 * @param {Node} node - The input node.
 * @returns {Node}
 */
function uncle(node) {
	const g = grandparent(node);
	// assert( g !== null ) ;
	// this can never happen
	if (node.parent === g.left)
		return g.right.isLeaf() ? null : g.right;

	return g.left.isLeaf() ? null : g.left;
}
//------------------------------------------------------------------------------
/**
 * Rotate tree left. (see https://en.wikipedia.org/wiki/Tree_rotation)
 * /!\ This swaps the references to A and B.
 *
 *         A                B
 *        / \              / \
 *       a   B     ->     A   c
 *          / \          / \
 *         b   c        a   b
 *
 *
 * @param {Node} A - The root of the tree.
 *
 */
function rotateLeft(A) {
	const B = A.right;
	const a = A.left;
	const b = B.left;
	const c = B.right;

	[A.key, B.key] = [B.key, A.key];
	[A.value, B.value] = [B.value, A.value];
	[A.color, B.color] = [B.color, A.color];

	A.left = B;
	A.right = c;

	B.left = a;
	B.right = b;

	a.parent = B;
	b.parent = B;
	c.parent = A;
}
//------------------------------------------------------------------------------
/**
 * Rotate tree right. (see https://en.wikipedia.org/wiki/Tree_rotation)
 * /!\ This swaps the references to A and B.
 *
 *         B                A
 *        / \              / \
 *       A   c     ->     a   B
 *      / \                  / \
 *     a   b                b   c
 *
 *
 * @param {Node} B - The root of the tree.
 *
 */
function rotateRight(B) {
	const A = B.left;
	const a = A.left;
	const b = A.right;
	const c = B.right;

	[A.key, B.key] = [B.key, A.key];
	[A.value, B.value] = [B.value, A.value];
	[A.color, B.color] = [B.color, A.color];

	B.left = a;
	B.right = A;

	A.left = b;
	A.right = c;

	a.parent = B;
	b.parent = A;
	c.parent = A;
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *   - n is not the root of the tree.
 *   - n's parent is red.
 *   - n's uncle is black.
 *   - the path from n to its grandparent makes a left-left or right-right.
 *
 * @param {Node} n - The input node.
 */
function insertCase5(n) {
	const g = grandparent(n);

	// repaint n's parent black, n's grandparent red
	n.parent.color = BLACK;
	g.color = RED;

	/**
	 * If the path from g to n makes a left-left, {@link rotateRight} at g.
	 * We are done.
	 *
	 *             R                     B
	 *           /   \                 /   \
	 *         B       B            >R       R
	 *        / \     / \   -->     / \     / \
	 *      >R   =   -   -         =   =   =   B
	 *      / \                               / \
	 *     =   =                             -   -
	 */
	if (n === n.parent.left)
		rotateRight(g);
	/**
	 * If the path from g to n makes a right-right, {@link rotate_left} at g.
	 * We are done.
	 *
	 *             R                     B
	 *           /   \                 /   \
	 *         B       B             R      >R
	 *        / \     / \   -->     / \     / \
	 *       -   -   =  >R         B   =   =   =
	 *                  / \       / \
	 *                 =   =     -   -
	 */
	else
		rotateLeft(g);
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *   - n is not the root of the tree.
 *   - n's parent is red.
 *   - n's uncle is black.
 *
 * Here we fix the input subtree to pass the preconditions of {@link insertCase5}.
 *
 * @param {Node} n - The input node.
 */
function insertCase4(n) {
	const g = grandparent(n);

	/**
	 * If the path from g to n makes a left-right, change it to a left-left
	 * with {@link rotate_left}. Then call {@link insertCase5} on the old
	 * parent of n.
	 *
	 *             B                     B
	 *           /   \                 /   \
	 *         R       B             R       B
	 *        / \     / \   -->     / \     / \
	 *       =  >R   -   -        >R   =   -   -
	 *          / \               / \
	 *         =   =             =   =
	 */

	if ((n === n.parent.right) && (n.parent === g.left)) {
		rotateLeft(n.parent);

		/**
		 * rotate_left can be the below because of already having *g =  grandparent(n)
		 *
		 * saved_p=g.left, *saved_left_n=n.left;
		 * g.left=n;
		 * n.left=saved_p;
		 * saved_p.right=saved_left_n;
		 *
		 * and modify the parent's nodes properly
		 */

		// n = n.left; /!\ need to fix rotate, so that we can safely reference a node
	}

	/**
	 * If the path from g to n makes a right-left, change it to a right-right
	 * with {@link rotateRight}. Then call {@link insertCase5} on the old
	 * parent of n.
	 *
	 *             B                     B
	 *           /   \                 /   \
	 *         B       R             B       R
	 *        / \     / \   -->     / \     / \
	 *       -   -  >R   =         -   -   =  >R
	 *              / \                       / \
	 *             =   =                     =   =
	 */

	else if ((n === n.parent.left) && (n.parent === g.right)) {
		rotateRight(n.parent);

		/**
		 * rotateRight can be the below to take advantage of already having *g =  grandparent(n)
		 *
		 * saved_p=g.right, *saved_right_n=n.right;
		 * g.right=n;
		 * n.right=saved_p;
		 * saved_p.left=saved_right_n;
		 *
		 */

		// n = n.right ;
	}

	insertCase5(n);
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *   - n is not the root of the tree.
 *   - n's parent is red.
 *
 * @param {Node} n - The input node.
 */
function insertCase3(n) {
	const u = uncle(n);

	/**
	 * If n has a non-leaf uncle and this uncle is red then we simply
	 * repaint the parent and the uncle of n in black, the grandparent of
	 * n in red, then call insertCase1 on n's grandparent.
	 *
	 *             B                    >R
	 *           /   \                 /   \
	 *         R       R             B       B
	 *        / \     / \   -->     / \     / \
	 *      >R   -   -   -         R   -   -   -
	 *      / \                   / \
	 *     -   -                 -   -
	 */

	if ((u !== null) && (u.color === RED)) {
		n.parent.color = BLACK;
		u.color = BLACK;
		const g = grandparent(n);
		g.color = RED;
		insertCase1(g);
	}
	else
		insertCase4(n);
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *   - n is not the root of the tree.
 *
 * @param {Node} n - The input node.
 */
function insertCase2(n) {
	/**
	 * If the parent of n is black then we have nothing to do.
	 *
	 *         B
	 *        / \
	 *      >R   -
	 *      / \
	 *     -   -
	 */
	if (n.parent.color === BLACK)
		return;

	insertCase3(n);
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is red.
 *
 * @param {Node} n - The input node.
 */
function insertCase1(n) {
	/**
	 * If n is the root of the tree, paint it black and we are done.
	 *
	 *      >R
	 *      / \
	 *     -   -
	 */
	if (n.parent === null)
		n.color = BLACK;
	else
		insertCase2(n);

}
//------------------------------------------------------------------------------
/**
 * Replaces node <code>A</code> by node <code>B</code>.
 *
 * @param {Node} A - The node to replace.
 * @param {Node} B - The replacement node.
 */
function replaceNode(A, B) {
	// assert( A.parent !== null ) ;
	// we never apply deleteOneChild on the root so we are safe

	if (A === A.parent.left)
		A.parent.left = B;
	else
		A.parent.right = B;

	B.parent = A.parent;
}
//------------------------------------------------------------------------------
/**
 * Computes the sibling of the input node.
 *
 * @param {Node} node - The input node.
 * @returns {Node}
 */
function sibling(node) {
	// assert((node !== null) && (node.parent !== null));
	// We only use this function when node HAS a sibling so this case can never
	// happen.
	return node === node.parent.left
		? node.parent.right
		: node.parent.left;
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *   - n's sibling is black
 *   - if n is a left child, the right child of n's sibling is red
 *   - if n is a right child, the left child of n's sibling is red
 *
 * @param {Node} n - The input node.
 */
function deleteCase6(n) {
	const s = sibling(n);

	/**
	 * Increment the black height of all root-leaf paths going through n by
	 * rotating at n's parent. This decrements the black height of all
	 * root-leaft paths going through n's sibling's right child.
     * We can repaint n's sibling's right child in black to fix this.
     * We are done.
	 *
	 *           ?                          ?
	 *        /     \                     /   \
	 *      >B        B                 B       B
	 *      / \      / \               / \     / \
	 *     -   -   =     R     -->   >B   =   =   B
	 *                  / \          / \         / \
	 *                 =   B        -   -       -   -
	 *                    / \
	 *                   -   -
	 */

	s.color = n.parent.color;
	n.parent.color = BLACK;

	if (n === n.parent.left) {
		s.right.color = BLACK;
		rotateLeft(n.parent);
	}
	// symmetric case
	else {
		s.left.color = BLACK;
		rotateRight(n.parent);
	}
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *   - n's sibling is black
 *   - at least one of n's sibling's children is red
 *
 * @param {Node} n - The input node.
 */
function deleteCase5(n) {
	const s = sibling(n);

	// The following statements just force the red n's sibling child to be on
	// the left of the left of the parent, or right of the right, so case 6
	// will rotate correctly.

	/**
	 *           ?                       ?
	 *         /   \                  /     \
	 *      >B       B              >B        B
	 *      / \     / \     -->     / \      / \
	 *     -   -  R     B          -   -   =     R
	 *           / \   / \                      / \
	 *          =   = -   -                    =   B
	 *                                            / \
	 *                                           -   -
	 */
	if ((n === n.parent.left) && (s.right.color === BLACK)) {
		s.color = RED;
		s.left.color = BLACK;
		rotateRight(s);
	}
	/**
	 *           ?                       ?
	 *         /   \                  /     \
	 *       B      >B               B       >B
	 *      / \     / \     -->     / \      / \
	 *    B     R  -   -          R     =   -   -
	 *   / \   / \               / \
	 *  -   - =   =             B   =
	 *                         / \
	 *                        -   -
	 */
	else if ((n === n.parent.right) && (s.left.color === BLACK)) {
		s.color = RED;
		s.right.color = BLACK;
		rotateLeft(s);
	}

	deleteCase6(n);
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *   - n's sibling is black
 *   - n's parent and n's sibling's children cannot all be black
 *
 * @param {Node} n - The input node.
 */
function deleteCase4(n) {
	const s = sibling(n);

	/**
	 * If n's parent is red and n's sibling's children are black, then swap n's
	 * parent and n's sibling color. All root-leaf paths going through n have
	 * now a black height of b. All other root-leaf paths have their black
	 * height unchanged. Red-black properties are respected. We are done.
	 *
	 *           R                       B
	 *         /   \                  /     \
	 *      >B       B              >B        R
	 *      / \     / \     -->     / \      / \
	 *     -   -  B     B          -   -   B     B
	 *           / \   / \                / \   / \
	 *          -   - -   -              -   - -   -
	 */
	// the parent color test is always true when coming from case 2
	if (
		(n.parent.color === RED) &&
		(s.left.color === BLACK) &&
		(s.right.color === BLACK)
	) {
		s.color = RED;
		n.parent.color = BLACK;
	}
	// Otherwise, go to case 5.
	else
		deleteCase5(n);
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *   - n's sibling is black
 *
 * @param {Node} n - The input node.
 */
function deleteCase3(n) {
	const s = sibling(n);

	/**
	 * If n's parent is black and n's sibling's children are black, then
     * repaint n's sibling red. Now all root-leaft paths going through n's
     * parent have a black height of b - 1. We recurse thus on n's parent.
	 *
	 *           B                      >B
	 *         /   \                  /     \
	 *      >B       B               B       R
	 *      / \     / \     -->    /   \    / \
	 *     -   -  B     B         -     - B     B
	 *           / \   / \               / \   / \
	 *          -   - -   -             -   - -   -
	 */
	if (
		(n.parent.color === BLACK) &&
		(s.left.color === BLACK) &&
		(s.right.color === BLACK)
	) {
		s.color = RED;
		deleteCase1(n.parent);
	}
	// Otherwise, go to case 4.
	else
		deleteCase4(n);
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *   - n is not the root
 *
 * @param {Node} n - The input node.
 */
function deleteCase2(n) {
	const s = sibling(n);

	/**
	 * If n's sibling is red, prepare for and go to case 4.
	 *
	 *           B                       B
	 *         /   \                  /     \
	 *      >B       R               R       B
	 *      / \     / \     -->    /   \    / \
	 *     -   -  B     B        >B     B  =   =
	 *           / \   / \       / \   / \
	 *          =   = =   =     -   - =   =
	 */
	if (s.color === RED) {
		n.parent.color = RED;
		s.color = BLACK;

		if (n === n.parent.left)
			rotateLeft(n.parent);
		else
			rotateRight(n.parent);

		deleteCase4(n);
	}
	// Otherwise, go to case 3.
	else
		deleteCase3(n);
}
//------------------------------------------------------------------------------
/**
 * Preconditions:
 *   - n is black
 *   - all root-leaf paths going through n have a black height of b - 1
 *   - all other root-leaf paths have a black height of b
 *
 * @param {Node} n - The input node.
 */
function deleteCase1(n) {
	// If n is the root, there is nothing to do: all paths go through n, and n
	// is black.
	if (n.parent !== null)
		deleteCase2(n);
}
//------------------------------------------------------------------------------
/**
 * Delete a node <code>n</code> that has at most a single non-leaf child.
 *
 * Precondition:
 *   - n has at most one non-leaf child.
 *   - if n has a non-leaf child, then it is its left child.
 *
 * @param {Node} n - The node to delete.
 */
function deleteOneChild(n) {
	// Precondition: n has at most one non-leaf child.
	// assert( n.right.isLeaf() || n.left.isLeaf());

	// const child = n.right.isLeaf() ? n.left : n.right;
	// n.right is always a LEAF because either n is a subtree predecessor or it
	// is the only child of its parent by the red-black tree properties
	const child = n.left;

	// replace n with its left child
	replaceNode(n, child);

	// If n is black, deleting it reduces the black-height of every path going
	// through it by 1.
	if (n.color === BLACK) {

		// We can easily fix this when its left child is an
		// internal red node: change the color of the left child to black and
		// replace n with it.
		if (child.color === RED)
			child.color = BLACK;

		// Otherwise, there are more things to fix.
		else
			deleteCase1(child);
	}
	// else {
	//    If n is red then its child can only be black. Replacing n with its
	//    child suffices.
	// }
}
//------------------------------------------------------------------------------
/**
 * Search for the first node whose key equals <code>key</code>.
 *
 * @param {Function} compare - The comparison function.
 * @param {Node} root - The root of the tree to scan.
 * @param {Key} key - The key to search for.
 * @returns {Node}
 */
function search(compare, root, key) {
	for (; ;) {
		const d = compare(key, root.key);

		if (d === 0)
			return root;

		if (d < 0)
			root = root.left;
		else
			root = root.right;

		if (root.isLeaf())
			return null;
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
// All keys ≤ x         IDBKeyRange.upperBound(x)
// All keys < x         IDBKeyRange.upperBound(x, true)
// All keys ≥ y         IDBKeyRange.lowerBound(y)
// All keys > y         IDBKeyRange.lowerBound(y, true)
// All keys ≥ x && ≤ y  IDBKeyRange.bound(x, y)
// All keys > x && < y  IDBKeyRange.bound(x, y, true, true)
// All keys > x && ≤ y  IDBKeyRange.bound(x, y, true, false)
// All keys ≥ x && < y  IDBKeyRange.bound(x, y, false, true)
// The key = z          IDBKeyRange.only(z)
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class KeyRange {
	constructor(x, y) {
		this.x = x;
		this.y = arguments.length < 2 ? x : y;
	}

	lowerBound(key, compare) {
		return compare(key, this.x) >= 0;
	}

	upperBound(key, compare) {
		return compare(key, this.y) <= 0;
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class KeyRangeAll {
	lowerBound(key, compare) {
		return true;
	}

	upperBound(key, compare) {
		return true;
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
class Walker {
	constructor(root, packer, range, compare) {
		if (root !== null) {
			this.path = [{
				node: root,
				way: -1
			}];
			this.sp = 0;
		}
		else
			this.sp = -1;

		this.it = {};
		this.packValue = packer;
		this.range = range;
		this.compare = compare;
	}

	[Symbol.iterator]() {
		return this;
	}

	next() {
		const { path, range, compare, it } = this;
		let t, p, { sp } = this;

		it.value = undefined;

		while (sp >= 0) {
			p = path[sp];

			if (p.way < 0) {
				p.way++;

				if (!p.node.left.isLeaf()) {
					t = path[sp + 1];

					if (t === undefined)
						path[sp + 1] = t = {};

					t.node = p.node.left;
					t.way = -1;
					sp++;
				}
			}
			else if (p.way > 1) {
				sp--;
			}
			else if (p.way > 0) {
				p.way++;

				if (!p.node.right.isLeaf()) {
					t = path[sp + 1];

					if (t === undefined)
						path[sp + 1] = t = {};

					t.node = p.node.right;
					t.way = -1;
					sp++;
				}
			}
			else {
				p.way++;

				if (range.lowerBound(p.node.key, compare)
					&& range.upperBound(p.node.key, compare)) {
					it.value = this.packValue(p.node);
					break;
				}
			}
		}

		this.sp = sp;
		it.done = sp < 0;

		return it;
	}
}
//------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
/**
 * A RedBlackTree with key-only nodes.
 *
 */
class RedBlackTree {

	/**
	 * Constructs a new empty red-black tree.
	 *
	 * @param {Function} compare - The comparison function for node keys.
	 * @returns {RedBlackTree}
	 */
	constructor(compare) {
		if (compare !== undefined)
			this.compare = compare;

		this.root = null;
	}

	compare(a, b) {
		const c = a > b ? 1 : (a < b ? -1 : 0);

		if (c === undefined)
			throw Error('Invalid comparison');

		return c;
	}

	/**
	 * Adds a key to the tree.
	 *
	 * @param {Key} key - The key to add.
	 * @param {Value} value - The value to add.
	 */
	add(key, value) {
		if (this.root === null) {
			this.root = new Node(BLACK, key, value);
			return true;
		}

		return insert(this.compare, this.root, key, value);
	}

	/**
	 * Set key in the tree.
	 *
	 * @param {Key} key - The key to add.
	 * @param {Value} value - The value to add.
	 */
	set(key, value) {
		if (this.root === null) {
			this.root = new Node(BLACK, key, value);
			return true;
		}

		return insert(this.compare, this.root, key, value, true);
	}

	/**
	 * Search for the input key in the tree.
	 * Returns the first node whose key equals the input key.
	 * If no such node exists, returns <code>null</code>.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Node}
	 */
	__search(key) {
		if (this.root === null)
			return null;

		return search(this.compare, this.root, key);
	}

	/**
	 * Search for the input key in the tree. Returns the first node key found
	 * in this way (with {@link RedBlackTree#_search}. If no such key exists
	 * in the tree, returns <code>null</code>.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Value}
	 */
	get(key) {
		const node = this.__search(key);
		return node === null ? null : node.value;
	}

	/**
	 * Returns <code>true</code> if and only if the tree contains the input
	 * key.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Boolean}
	 */
	has(key) {
		return this.__search(key) !== null;
	}

	/**
	 * Deletes the input node from the tree.
	 *
	 * @param {Node} node - The input node to delete.
	 */
	__delete(node) {
		if (!node.left.isLeaf()) {
			// replace node's key with predecessor's key
			const pred = predecessor(node);
			node.key = pred.key;
			node.value = pred.value;
			// delete predecessor node
			// note: this node can only have one non-leaf child
			//       because the tree is a red-black tree
			deleteOneChild(pred);
		}
		else if (!node.right.isLeaf()) {
			// replace node's key with successor's key
			// If there is no left child, then there can only be one right
			// child.
			const succ = node.right;
			node.key = succ.key;
			node.value = succ.value;
			// delete successor node
			// note: this node can only have one non-leaf child
			//       because the tree is a red-black tree
			deleteOneChild(succ);
		}
		else if (node === this.root) {
			this.root = null;
		}
		else {
			deleteOneChild(node);
		}
	}

	/**
	 * Search for the first node of the tree whose key equals the input key
	 * (with {@link RedBlackTree#_search}), then delete that node
	 * (with {@link RedBlackTree#_delete}). If such a node is found and deleted
	 * then return <code>true</code>. Return <code>false</code> otherwise.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Boolean} - Whether the key existed in the tree before removal.
	 */
	remove(key) {
		const node = this.__search(key);

		if (node === null)
			return false;

		this.__delete(node);

		return true;
	}

	/**
	 * Search for the first node of the tree whose key equals the input key
	 * (with {@link RedBlackTree#_search}), then remove that node
	 * (with {@link RedBlackTree#_delete}). If such a node is found and deleted
	 * then return <code>true</code>. Return <code>false</code> otherwise.
	 *
	 * @param {Key} key - The input key.
	 * @returns {Boolean} - Whether the key existed in the tree before removal.
	 */
	delete(key) {
		const node = this.__search(key);

		if (node === null)
			return false;

		this.__delete(node);

		return true;
	}

	/**
	 * Same as {@link RedBlackTree#values}.
	 */
	[Symbol.iterator]() {
		return this.values();
	}

	/**
	 * Returns an in order iterator over the keys of the tree.
	 *
	 * @returns {Iterator}
	 */
	keys() {
		return new Walker(this.root, this.__keysPacker);
	}

	__keysPacker(n) {
		return n.key;
	}

	/**
	 * Returns an in order iterator over the values of the tree.
	 *
	 * @returns {Iterator}
	 */
	values() {
		return new Walker(this.root, this.__valuesPacker);
	}

	__valuesPacker(n) {
		return n.value;
	}

	/**
	 * Returns an in order iterator over the entries of the tree.
	 *
	 * @returns {Iterator}
	 */
	entries(range, compare) {
		if (!range)
			range = new KeyRangeAll();
		if (!compare)
			compare = this.compare;

		return new Walker(this.root, this.__entriesPacker, range, compare);
	}

	__entriesPacker(n) {
		return [n.key, n.value, n];
	}
}
//------------------------------------------------------------------------------
RedBlackTree.KeyRange = KeyRange;
RedBlackTree.KeyRangeAll = KeyRangeAll;
export default RedBlackTree;
//------------------------------------------------------------------------------
