//------------------------------------------------------------------------------
import hash, { hash12 } from './hash';
//------------------------------------------------------------------------------
export default class HashMap {
	constructor(...args) {
		this.clear();

		if (args.length > 0 && args[0][Symbol.iterator]) {
			for (const [key, value] of args[0])
				this.set(key, value);
		}
	}

	get size() {
		return this._size;
	}

	[Symbol.iterator]() {
		return this.entries();
	}

	clear() {
		this._size = 0;
		this._slots = [null];
	}

	entries() {
		const e = [];

		for (let item of this._slots)
			while (item !== null) {
				e.push([item.key, item.value]);
				item = item.next;
			}

		return e;
	}

	keys() {
		const e = [];

		for (let item of this._slots)
			while (item !== null) {
				e.push(item.key);
				item = item.next;
			}

		return e;
	}

	values() {
		const e = [];

		for (let item of this._slots)
			while (item !== null) {
				e.push(item.value);
				item = item.next;
			}

		return e;
	}

	forEach(functor) {
		for (const { value, key } of this.entries())
			functor(value, key, this);
	}

	has(key) {
		return this.__find(key) !== null;
	}

	get(key) {
		const item = this.__find(key);
		return item !== null ? item.value : undefined;
	}

	set(key, value) {
		const { _slots } = this;
		const h = this.__hash(key);
		const mask = _slots.length - 1;
		const slot = h & mask;
		let item = _slots[slot], next = item;

		if (item === null) {
			_slots[slot] = { hash: h, key, value, next };
		}
		else {
			do {
				item = next;

				if (this.__equals(key, item.key)) {
					item.value = value;
					return this;
				}

				next = item.next;
			} while (next !== null);

			item.next = { hash: h, key, value, next };
		}

		const count = _slots.length << 1;
		const goldenRatio = this.__goldenRatio(count);

		if (++this._size > count + goldenRatio)
			this.__rehash(_slots.length << 1);

		return this;
	}

	delete(key) {
		const { _slots } = this;
		const h = this.__hash(key);
		const mask = _slots.length - 1;
		const slot = h & mask;
		let item = _slots[slot], prev = null;

		while (item !== null) {
			if (this.__equals(key, item.key))
				break;

			prev = item;
			item = item.next;
		}

		if (item !== null) {
			if (prev !== null)
				prev.next = item.next;
			else
				_slots[slot] = item.next;

			const count = _slots.length << 1;
			const goldenRatio = this.__goldenRatio(count);

			if (--this._size < count - goldenRatio)
				this.__rehash(_slots.length >>> 1);

			return true;
		}

		return false;
	}

	__equals(key1, key2) {
		let equ;

		if (Array.isArray(key1)) {
			equ = false;

			if (Array.isArray(key2) && key1.length === key2.length) {
				equ = true;

				for (let i = key1.length - 1; i >= 0; i--)
					if ((equ = this.__equals(key1[i], key2[i])) === false)
						break;
			}
		}
		else if (key1 !== undefined && key1 !== null && key1 !== Infinity && !Number.isNaN(key1) && key1.equals) {
			equ = key1.equals(key2);
		}
		else if (key2 !== undefined && key2 !== null && key2 !== Infinity && !Number.isNaN(key2) && key2.equals) {
			equ = key2.equals(key1);
		}
		else {
			equ = key1 === key2;
		}

		return !!equ;
	}

	__hash(key, h12 = hash12()) {
		let h;

		if (Array.isArray(key)) {
			for (const k of key)
				h = this.__hash(k, h12);
		}
		else if (key === undefined){
			// \x98 - SOS, Start of String, ASCII C1 control code
			// \x9C -  ST, String terminator, ASCII C1 control code
			// \x1A - SUB, ASCII C0 control code
			// \x04 - EOT, ASCII C0 control code
			h = hash('\x1A\x98js:undefined\x9C\x04', h12);
		}
		else if (key === null){
			h = hash('\x1A\x98js:null\x9C\x04', h12);
		}
		else if (key === Infinity) {
			h = hash('\x1A\x98js:Infinity\x9C\x04', h12);
		}
		else if (Number.isNaN(key)) {
			h = hash('\x1A\x98js:NaN\x9C\x04', h12);
		}
		else if (key.hash) {
			h = key.hash(h12);
		}
		else {
			h = hash(key, h12);
		}

		return h;
	}

	__find(key) {
		const { _slots } = this;
		const h = this.__hash(key);
		const mask = _slots.length - 1;
		let item = _slots[h & mask];

		while (item !== null) {
			if (this.__equals(key, item.key))
				return item;

			item = item.next;
		}

		return null;
	}

	__rehash(size) {
		if (size > 0) {
			const mask = size - 1;
			const slots = [];

			while (slots.length < size)
				slots.push(null);

			for (let entry of this._slots)
				while (entry !== null) {
					const next = entry.next;
					const slot = entry.hash & mask;
			
					entry.next = slots[slot];
					slots[slot] = entry;
					entry = next;
				}

			this._slots = slots;
		}
	}

	__goldenRatio(count) {
		return ((count << 2) + count) // mul 5
			>>> 3; // div 8
	}
}
//------------------------------------------------------------------------------
