//------------------------------------------------------------------------------
const root = function () {
	const obj = 'object';

	if (typeof window === obj)
		return window;
	if (typeof document === obj)
		return document;
	if (typeof self === obj)
		return self;
	if (typeof global === obj)
		return global;
	if (typeof navigator === obj)
		return navigator;
}();
//------------------------------------------------------------------------------
export default root;
//------------------------------------------------------------------------------
