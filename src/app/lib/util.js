//------------------------------------------------------------------------------
import { route } from 'preact-router';
import root from './root';
import strftime from './strftime';
import sprintf from './sprintf';
//------------------------------------------------------------------------------
export function isIterable(v) {
	return v !== undefined
		&& v !== null
		&& v !== Infinity
		&& !Number.isNaN(v)
		&& (v[Symbol.iterator] instanceof Function
			|| (v[Symbol.iterator]
				&& v[Symbol.iterator].constructor === Function));
}
//------------------------------------------------------------------------------
export function isPrimitiveValue(v) {
	return v === undefined
		|| v === null
		|| v === Infinity
		|| Number.isNaN(v)
		|| v.constructor === Boolean || v instanceof Boolean
		|| v.constructor === String || v instanceof String
		|| v.constructor === Number || v instanceof Number
		|| v.constructor === Symbol || v instanceof Symbol;
}
//------------------------------------------------------------------------------
export function sscat(delimiter, ...args) {
	let s = '';

	for (const arg of args) {
		if (isPrimitiveValue(arg))
			continue;

		const a = arg.toString().trim();

		if (a.length !== 0)
			s += delimiter + a;
	}

	return s.substr(delimiter.length).trim();
}
//------------------------------------------------------------------------------
export function isArrow(functor) {
	return functor
		&& (functor.constructor === Function || functor instanceof Function)
		&& !functor.hasOwnProperty('prototype');
}
//------------------------------------------------------------------------------
export function equal(a, b) {
	return a === b;
}
//------------------------------------------------------------------------------
export function neq(a, b) {
	return a === b;
}
//------------------------------------------------------------------------------
export function alwaysNotEqual() {
	return false;
}
//------------------------------------------------------------------------------
export function shallowEqual(a, b, equ = equal) {
	if (a === b)
		return true;

	let i, isA = Array.isArray(a), isB = Array.isArray(b);

	if (isA && isB) {
		if (a.length !== b.length)
			return false;

		for (i = a.length - 1; i >= 0; i--)
			if (!equ(a[i], b[i], equ))
				return false;

		return true;
	}

	if (isA !== isB)
		return false;

	isA = ~~isPrimitiveValue(a);
	isB = ~~isPrimitiveValue(b);

	// XOR
	if ((isA ^ isB) !== 0)
		return false;

	if (isA !== 0 && isB !== 0)
		return a === b;

	isA = a.constructor === Set || a instanceof Set;
	isB = b.constructor === Set || b instanceof Set;

	if (isA && isB) {
		if (a.size !== b.size)
			return false;

		const values = a.values();

		for (const v of values)
			if (!equ(v, b.get(v), equ))
				return false;
	}

	if (isA !== isB)
		return false;

	isA = a.constructor === Map || a instanceof Map;
	isB = b.constructor === Map || b instanceof Map;

	if (isA && isB) {
		if (a.size !== b.size)
			return false;

		const keys = a.keys();

		for (const key of keys)
			if (!equ(a.get(key), b.get(key), equ))
				return false;
	}

	if (isA !== isB)
		return false;

	isA = a.constructor === Date || a instanceof Date;
	isB = b.constructor === Date || b instanceof Date;

	if (isA && isB)
		return a.getTime() === b.getTime();

	if (isA !== isB)
		return false;

	isA = a.constructor === RegExp || a instanceof RegExp;
	isB = b.constructor === RegExp || b instanceof RegExp;

	if (isA && isB)
		return a.toString() === b.toString();

	if (isA !== isB)
		return false;

	isA = a.constructor === Object || a instanceof Object;
	isB = b.constructor === Object || b instanceof Object;

	if (isA && isB) {
		const keys = Object.keys(a);

		if (keys.length !== Object.keys(b).length)
			return false;

		for (i = keys.length - 1; i >= 0; i--) {
			const key = keys[i];

			if (!equ(a[key], b[key], equ))
				return false;
		}

		return true;
	}

	if (isA !== isB)
		return false;

	return false;
}
//------------------------------------------------------------------------------
export function deepEqual(a, b) {
	return shallowEqual(a, b, deepEqual);
}
//------------------------------------------------------------------------------
export function difference(oldObject, newObject, equ = equal) {
	const keys = new Set([
		...Object.keys(newObject),
		...Object.keys(oldObject)
	]);
	const diff = {};

	for (const k of keys)
		if (!equ(oldObject[k], newObject[k]))
			diff[k] = [oldObject[k], newObject[k]];

	return Object.keys(diff).length !== 0 ? diff : undefined;
}
//------------------------------------------------------------------------------
export function copy(src) {
	let dst = src;

	if (src === undefined || src === null) {
	}
	else if (Array.isArray(src)) {
		dst = [];

		for (const v of src)
			dst.push(copy(v));
	}
	else if (src.constructor === Map || src instanceof Map) {
		dst = new Map();

		for (const [k, v] of src.entries())
			dst.set(copy(k), copy(v));
	}
	else if (src.constructor === Set || src instanceof Set) {
		dst = new Set();

		for (const v of src.values())
			dst.add(copy(v));
	}
	else if (src.constructor === String || src instanceof String)
		dst = src.valueOf();
	else if (src.constructor === Number || src instanceof Number)
		dst = src.valueOf();
	else if (src.constructor === Boolean || src instanceof Boolean)
		dst = src.valueOf();
	else if (src.constructor === Date || src instanceof Date)
		dst = new Date(src.valueOf());
	else if (src.constructor === Object || src instanceof Object) {
		dst = new src.constructor();

		for (const n of Object.keys(src))
			dst[n] = copy(src[n]);
	}
	else
		dst = new src.constructor(src.valueOf());

	return dst;
}
//------------------------------------------------------------------------------
export function encodeURIString(val) {
	return encodeURIComponent(val)
		.replace(/%40/gi, '@')
		.replace(/%3A/gi, ':')
		.replace(/%24/g, '$')
		.replace(/%2C/gi, ',')
		//.replace(/%20/g, '+')
		.replace(/%5B/gi, '[')
		.replace(/%5D/gi, ']');
}
//------------------------------------------------------------------------------
export function encodeURIParameter(k, v) {
	if (v.constructor === Date || v instanceof Date)
		v = v.toISOString();
	else if (v.constructor === Object || v instanceof Object)
		v = JSON.stringify(v);
	else if (v.constructor === Set || v instanceof Set)
		v = JSON.stringify([...v]);
	else if (v.constructor === Map || v instanceof Map)
		v = JSON.stringify([...v]);
	return encodeURIString(k) + '=' + encodeURIString(v);
}
//------------------------------------------------------------------------------
export function serializeURIParams(params) {
	const parts = [];

	for (let key of Object.keys(params)) {
		let val = params[key];

		if (val === undefined || val === null)
			continue;

		const isArray = Array.isArray(val);

		if (isArray)
			key += '[]';
		else
			val = [val];

		for (const v of val)
			parts.push(encodeURIParameter(key, v));
	}

	return parts.join('&');
}
//------------------------------------------------------------------------------
export function stopPropagation(e) {
	if (e) {
		if (e.stopImmediatePropagation)
			e.stopImmediatePropagation();

		if (e.stopPropagation)
			e.stopPropagation();

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
export function prevent(e) {
	if (e) {
		if (e.stopImmediatePropagation)
			e.stopImmediatePropagation();

		if (e.stopPropagation)
			e.stopPropagation();

		e.preventDefault();
	}

	return false;
}
//------------------------------------------------------------------------------
export function pRoute(e, path) {
	route(path);
	return prevent(e);
}
//------------------------------------------------------------------------------
export function plinkRoute(path) {
	return e => pRoute(e, path);
}
//------------------------------------------------------------------------------
export function randomInteger(min = 0, max = Number.MAX_SAFE_INTEGER) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
//------------------------------------------------------------------------------
// Intl not work correctly under android
//const dateFormatter = date => {
//	let f = new Intl.DateTimeFormat('ru-RU', {
//		year	: 'numeric',
//		month	: '2-digit',
//		day		: '2-digit',
//		hour	: '2-digit',
//		minute	: '2-digit',
//		second	: '2-digit',
//		hour12	: false
//	});
//	return f.format(date);
//};
export function dateFormatter(date) {
	return strftime('%d.%m.%Y %H:%M:%S', date);
}
//------------------------------------------------------------------------------
const msp = 1000000; // timer precision
//------------------------------------------------------------------------------
export function ms() {
	//return (new Date).getTime();
	// don't forget set privacy.reduceTimerPrecision to false on about:config page
	const t = performance.now();
	const d = Math.trunc(t);
	const micro = d * 1000;
	const delta = t - d;
	const submicro = delta * 1000;
	return Math.trunc(micro + submicro);
}
//------------------------------------------------------------------------------
export function ellapsed(ms) {
	if (ms < 0)
		ms = 0;

	const a = Math.trunc(ms / msp);
	const days = Math.trunc(a / (60 * 60 * 24));
	const hours = Math.trunc(a / (60 * 60)) - days * 24;
	const mins = Math.trunc(a / 60) - days * 24 * 60 - hours * 60;
	const secs = a - days * 24 * 60 * 60 - hours * 60 * 60 - mins * 60;
	const msecs = ms % msp;
	let s;

	if (days !== 0)
		s = sprintf('%u:%02u:%02u:%02u.%06u', days, hours, mins, secs, msecs);
	else if (hours !== 0)
		s = sprintf('%u:%02u:%02u.%06u', hours, mins, secs, msecs);
	else if (mins !== 0)
		s = sprintf('%u:%02u.%06u', mins, secs, msecs);
	else if (secs !== 0)
		s = sprintf('%u.%06u', secs, msecs);
	else
		s = sprintf('.%06u', msecs);

	return s;
}
//------------------------------------------------------------------------------
export function htmlConsoleLog(...args) {
	let e = document.getElementById('consoleStyle');

	if (!e) {
		e = document.createElement('style');
		e.id = 'consoleStyle';
		document.head.appendChild(e);
		e.sheet.insertRule(
			`.console {
				line-height: 1.0;
			}`);
	}

	e = document.getElementById('console');

	if (!e) {
		e = document.createElement('pre');
		e.id = 'console';
		e.classList.add('console');
		document.body.appendChild(e);
	}

	e.innerHTML += args.join(' ') + '<br>';
}
//------------------------------------------------------------------------------
export function doFullScreen() {
	const doc = document;
	const isInFullScreen = (doc.fullScreenElement && doc.fullScreenElement !== null) ||
		(doc.mozFullScreen || doc.webkitIsFullScreen);

	if (!isInFullScreen) {
		const docElm = doc.documentElement;

		if (docElm.webkitRequestFullScreen) {
			docElm.webkitRequestFullScreen();
			//alert("Webkit entering fullscreen!");
		}
		else if (docElm.requestFullscreen) {
			docElm.requestFullscreen();
			//alert("Entering fullscreen!");
		}
		else if (docElm.mozRequestFullScreen) {
			docElm.mozRequestFullScreen();
			//alert("Mozilla entering fullscreen!");
		}
		else if (docElm.msRequestFullScreen) {
			docElm.msRequestFullScreen();
			//alert("EDGE entering fullscreen!");
		}
	}
}
//------------------------------------------------------------------------------
export function isEdge() {
	return root.navigator.appVersion.indexOf('Edge') !== -1;
}
//------------------------------------------------------------------------------
export function isFirefox() {
	// const keys = ['appName', 'appVersion', 'product', 'productSub', 'userAgent'];
	// //console.log(root.navigator);
	// const a = [];
	// for (const k of keys)
	// 	a.push(`${k}: ${root.navigator[k]}`);

	// console.log(...a);
	return root.navigator.userAgent.indexOf('Firefox') !== -1;
}
//------------------------------------------------------------------------------
export function xpathEval(path, parent = undefined, documentNode = null) {

	let doc = documentNode === null ? root.document : documentNode;
	let it = doc.evaluate(path, parent ? parent : doc, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
	let a = [], e;

	while ((e = it.iterateNext()))
		a.push(e);

	return a;
}
//------------------------------------------------------------------------------
export function xpathEvalSingle(path, parent = undefined, documentNode = null) {
	const a = xpathEval(path, parent, documentNode);

	if (a.length > 1)
		throw new Error('evaluate return multiple elements');

	if (a.length === 0)
		throw new Error('evaluate return no result');

	return a[0];

}
//------------------------------------------------------------------------------
