//------------------------------------------------------------------------------
// !!!! cut from here in forked project, needed only for example
//------------------------------------------------------------------------------
// raw-loader
// It is a loader that lets us import files as a string.
import template from './index.tpl.html';

let component = {
	template // Use ES6 enhanced object literals.
};

console.log('raw-loader', component);

class TestClass {
	constructor() {
		let msg = "Using ES2015+ syntax";
		console.log(msg);
	}
}

let test = new TestClass();

console.log('Running App version ' + VERSION, API_KEY);
//------------------------------------------------------------------------------
// !!!! and cut to here
//------------------------------------------------------------------------------
import './lib/swreg.js';
//------------------------------------------------------------------------------
import 'preact/debug';
import { render } from 'preact';
import './style.css';
import App from './components/App';
import { loader as stateLoader } from './lib/store';
import root from './lib/root';
//------------------------------------------------------------------------------
(function () {
	const event = 'DOMContentLoaded';
	const wakeupApp = () => {
		const ldr = document.getElementById('loader');

		if (ldr && ldr.parentNode)
			ldr.parentNode.removeChild(ldr);

		render(<App />, document.body);
	};
	const loader = () => {
		root.document.removeEventListener(event, loader);

		const globals = {
			backend: 'backend'
		};
		const exclude = [
			'header.spinner'
		];

		stateLoader(globals, exclude).then(wakeupApp);
	};

	if (/comp|inter|loaded/.test(document.readyState)) {
		// In case DOMContentLoaded was already fired, the document readyState
		// will be one of "complete" or "interactive" or (nonstandard) "loaded".
		// The regexp above looks for all three states. A more readable regexp
		// would be /complete|interactive|loaded/
		loader();
	}
	else {
		// In case DOMContentLoaded was not yet fired, use it to run the "start"
		// function when document is read for it.
		root.document.addEventListener(event, loader, false);
	}
})();
//------------------------------------------------------------------------------
