//------------------------------------------------------------------------------
import { Component } from 'preact';
import { Router } from 'preact-router';
import createHashHistory from 'history/createHashHistory';
import Button from 'preact-material-components/Button';
import 'preact-material-components/Button/style.css';
import 'preact-material-components/Theme/style.css';
import style from './style.scss';
//------------------------------------------------------------------------------
console.log('CSS variable mdcTopAppBarRowHeight: ' + style.mdcTopAppBarRowHeight);
console.log('CSS variable publicPath: ' + style.publicPath);
//------------------------------------------------------------------------------
/** fall-back route (handles unroutable URLs) */
const Error = ({ type, url }) => (
	<section class={style.tapbtp}>
		<h2>Error {type}</h2>
		<p>It looks like we hit a snag.</p>
		<pre>{url}</pre>
	</section>
);
//------------------------------------------------------------------------------
console.log(style.tapbtp);
const Home = ({ url }) => (
	<div class={style.tapbtp}>
		<h2>Home</h2>
		<p>Hello world from preact!!!</p>
		<pre>url: {url}</pre>
		<Button ripple raised>
			Flat button with ripple
		</Button>
	</div>
);
//------------------------------------------------------------------------------
export default class App extends Component {
	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => this.currentUrl = e.url

	getHashHistory() {
		// https://stackoverflow.com/questions/45742982/set-base-url-for-preact-cli
		const canUseDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

		if (canUseDOM)
			return createHashHistory();
	}

	render(props, state) {
		const content = [
			<Router onChange={this.handleRoute} history={this.getHashHistory()}>
				<Home path="/" />
				<Error type="404" default />
			</Router>
		];

		return <div id="app">{content}</div>;
	}
}
//------------------------------------------------------------------------------
import { bfetch } from '../../backend';