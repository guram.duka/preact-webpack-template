//------------------------------------------------------------------------------
import 'whatwg-fetch';
//import 'setimmediate';
//import Promise from 'promise-polyfill';
import Dexie from 'dexie';
import { LRUMap } from 'lru_map';
import hash from '../lib/hash';
import root from '../lib/root';
import transform from './transform';
import Cache from './cache';
import {
	encodeURIParameter,
	serializeURIParams
} from '../lib/util';
import { stringify, destringify } from '../lib/json';
//------------------------------------------------------------------------------
// const cacheLRU = new LRUMap(400);
// cacheLRU.__openDB = function (callback) {
// 	const request = root.indexedDB.open('backend-cache', 1); // Request version 1.

// 	request.onerror = function (e) {
// 		root.console.log('Why didn\'t you allow my web app to use IndexedDB?!');
// 	};

// 	request.onsuccess = e => {
// 		cacheLRU.__db = e.target.result;
// 		cacheLRU.__db.onerror = e => {
// 			// Generic error handler for all errors targeted at this database's
// 			// requests!
// 			root.console.log('Database error: ' + e.target.errorCode);
// 		};

// 		callback(cacheLRU.__db);
// 	};

// 	request.onupgradeneeded = e => {
// 		cacheLRU.__db = request.result;

// 		if (e.oldVersion < 1) {
// 			// Version 1 is the first version of the database.
// 			const store = cacheLRU.__db.createObjectStore('cache', { keyPath: 'pkey' });
// 			store.createIndex('by_atime', 'atime', { unique: false, autoIncrement: false });
// 			store.createIndex('by_ctime', 'ctime', { unique: false, autoIncrement: false });
// 			store.createIndex('by_mtime', 'mtime', { unique: false, autoIncrement: false });
// 		}
// 	};
// };

// cacheLRU.shift = function () {
// 	const entry = LRUMap.prototype.shift.call(this);
// 	const [key] = entry;

// 	// perform finalization of items as they are evicted from the cache
// 	// save entry to db

// 	const callback = db => {
// 		const tx = db.transaction('cache', 'readwrite');
// 		const store = tx.objectStore('cache');

// 		store.put(Object.assign({ value }, key));
// 		store.delete(IDBKeyRange.only(key.pkey));

// 		// tx.oncomplete = () => {
// 		// 	// All requests have succeeded and the transaction has committed.
// 		// };
// 	};

// 	if (cacheLRU.__db)
// 		callback(cacheLRU.__db);
// 	else
// 		cacheLRU.__openDB(callback);

// 	return entry;
// };

// cacheLRU.get = function (key) {
// 	if (this.has(key))
// 		return LRUMap.prototype.get.call(this, key);

// 	// load entry from db
// };
//------------------------------------------------------------------------------
function burl(opts) {
	if (opts.method === undefined)
		opts.method = 'GET';

	if (opts.credentials === undefined)
		opts.credentials = 'omit';

	if (opts.mode === undefined)
		opts.mode = 'cors';//root.backend.toLowerCase().startsWith('https') ? 'cors' : 'no-cors';

	if (opts.cache === undefined)
		opts.cache = 'default';

	const r = {};

	// need for caching authorized request separate from regular not authorized
	if (opts.r)
		r.r = opts.r;

	if (opts.rmod)
		opts.rmod(r);

	opts.url = root.backend;

	// resource cached by key of unique url and headers
	if (opts.method === 'GET') {
		let u = '';

		if (opts.a !== undefined)
			u += '&' + encodeURIParameter('a', opts.a);

		if (opts.e !== undefined)
			u += '&' + encodeURIParameter('e', opts.e);

		opts.url += '?' + serializeURIParams(r);

		if (u.length !== 0)
			opts.u = u;

		if (opts.auth && opts.auth.authorized && opts.a !== undefined) {
			if (!opts.headers)
				opts.headers = new Headers();

			opts.headers.append('X-Access-Data', opts.auth.link + ', ' + opts.auth.hash);
		}
	}
	else {
		opts.body = JSON.stringify(r.r);

		if (opts.auth && opts.auth.authorized) {
			if (!opts.headers)
				opts.headers = new Headers();

			opts.headers.append('X-Access-Data', opts.auth.link + ', ' + opts.auth.hash);
		}
	}

	return opts.url;
}
//------------------------------------------------------------------------------
burl.__getMaxAge = function (xMaxAge) {
	if (xMaxAge) {
		xMaxAge = xMaxAge.split(',').find(v => v.match(burl.__ma0));

		if (xMaxAge) {
			xMaxAge = xMaxAge.replace(burl.__ma1, '');
			if (xMaxAge)
				xMaxAge = ~~xMaxAge; // fast convert string to integer
		}
	}

	return xMaxAge;
};
//------------------------------------------------------------------------------
burl.__ma0 = /max-age/gi;
burl.__ma1 = /max-age|[= ]/gi;
//------------------------------------------------------------------------------
function pullNetwork(opts, url) {
	return new Dexie.Promise((resolve, reject) => {
		try {
			opts.controller = new root.AbortController();
			opts.signal = opts.controller.signal;
		}
		catch (e) {
			opts.controller = { abort: () => opts.aborting = true };
		}

		fetch(url, opts).then(response => {
			const contentType = response.headers.get('content-type');

			opts.responseHeaders = response.headers;

			if (response.ok && contentType) {
				if (contentType.includes('application/json'))
					return response.json();
				if (contentType.includes('text/'))
					return response.text();
				if (contentType.includes('image/'))
					return opts.blob
						? response.blob()
						: response.arrayBuffer();
			}

			// will be caught below
			throw new TypeError('Oops, we haven\'t right type of response! Status: '
				+ response.status + ', ' + response.statusText + ', content-type: ' + contentType);
		}).then(result => {
			if (result === undefined || result === null ||
				!(result.constructor === Object || result instanceof Object
					|| Array.isArray(result)
					|| result.constructor === ArrayBuffer || result instanceof ArrayBuffer))
				// will be caught below
				throw new TypeError('Oops, we haven\'t got data! ' + result);

			const xDate = opts.responseHeaders.get('date');

			if (xDate)
				result.date = new Date(xDate);

			const xMaxAge = burl.__getMaxAge(opts.responseHeaders.get('cache-control'));

			if (xMaxAge)
				result.maxAge = xMaxAge;

			const xEol = opts.responseHeaders.get('End-Of-Life');

			if (xEol) {
				result.endOfLife = new Date(xEol);
			}
			else if (xDate && xMaxAge) {
				const t = new Date(xDate);
				t.setTime(t.getTime() + xMaxAge * 1000);
				result.endOfLife = t;
			}

			resolve(result);
		}).catch(e => {
			if (e instanceof root.AbortError)
				opts.aborting = opts.aborted = true;
			else if (e instanceof Error)
				throw e;
			reject(e);
		});
	});
};
//------------------------------------------------------------------------------
function cutWeakDeps(result) {
	let { weakDeps } = result;

	if (weakDeps)
		weakDeps = transform(weakDeps).rows;

	delete result.weakDeps;

	return weakDeps;
}
//------------------------------------------------------------------------------
export function bfetch(opts) {
	let url = burl(opts);

	if (opts.u) {
		url += opts.u;
		opts.h = hash(opts.u);
	}

	opts.controller = { abort: () => opts.aborting = true };

	const resultHandler = result => {
		if (result.errorCode === 401) {
			//store.transform('auth', v => delete v.authorized);
		}
		else {
		}

		return result;
	};

	if (opts.method === 'GET')
		return Cache.readLine(opts.h, opts.u).then(line => {
			const { id, v } = line;
			const now = new Date();

			if (v.t < now) {
				//opts.headers.append('Cache-Control', '');
				return pullNetwork(opts, url).then(v => { // pullNetwork completed
					// don't wait write operation completed
					Cache.writeLine(opts.h, opts.u, v.endOfLife, v, id, cutWeakDeps(v));
					return v;
				});
			}

			// if cache line valid then return data immediately
			return resultHandler(v);
		});

	// special handling for data updated in current request,
	// need fetch fresh data in next requests with data weak linked
	return pullNetwork(opts, url).then(result => {
		// don't wait invalidate write operations completed
		Cache.invalidateLines(result, cutWeakDeps(result));
		return resultHandler(result);
	});
}
//------------------------------------------------------------------------------
export function imgReq(...args) {
	const [arg0] = args;
	const r = { m: 'image' };

	if (arg0.constructor === Object || arg0 instanceof Object) {
		for (const k of Object.keys(arg0))
			if (arg0[k] !== undefined)
				r[k] = arg0[k];
	}
	else
		[r.u, r.w, r.h] = args;

	return { r };
}
//------------------------------------------------------------------------------
export function imgUrl(...args) {
	const [arg0] = args;
	const s = root.backend + '?';
	const r = {};

	if (arg0.constructor === Object || arg0 instanceof Object) {
		for (const k of Object.keys(arg0))
			r[k] = arg0[k];
	}
	else
		[r.u, r.w, r.h] = args;

	return s + serializeURIParams(imgReq(r));
}
//------------------------------------------------------------------------------
export function imgKey(...args) {
	const r = imgReq(...args).r;
	const { u, w, h } = r;

	delete r.m;
	delete r.u;
	delete r.w;
	delete r.h;

	let key = 'i' + u.replace(/-/g, '');

	if (w !== undefined)
		key += '_' + w;

	if (h !== undefined) {
		if (w === undefined)
			key += '_';
		key += 'x' + h;
	}

	for (const k of Object.keys(r))
		if (r[k] !== undefined)
			key += '_' + k + '_' + r[k];

	return key;
}
//------------------------------------------------------------------------------
