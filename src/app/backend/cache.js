//------------------------------------------------------------------------------
import Dexie from 'dexie';
import HashMap from '../lib/hashmap';
//------------------------------------------------------------------------------
// static singleton
//------------------------------------------------------------------------------
export default class Cache {
	static db

	static __replaceLineWeakDeps(lineId, lineWeakDeps) {
		const { db } = Cache;
		const { weakDeps } = db;

		return weakDeps.where({ lineId }).toArray().then(items => {
			const lineWeakDepsMap = new HashMap(lineWeakDeps.map(
				e => [[e.type, e.key, e.value], { lineId, ...e }]
			));
			const delIds = [];

			for (const { id, type, key, value } of items) {
				const k = [type, key, value];

				if (lineWeakDepsMap.has(k))
					// no need add exists deps
					lineWeakDepsMap.delete(k);
				else
					// delete undefined deps
					delIds.push(id);
			}

			return [
				weakDeps.bulkDelete(delIds),
				weakDeps.bulkAdd(lineWeakDepsMap.values())
			];
		});
	}

	// only read line.
	static readLine(h, u) {
		const { db } = Cache;
		const { lines } = db;
		const uEqu = line => line.u === u;

		return db.transaction('r', [lines],
			// eslint-disable-next-line no-unused-vars
			tx => lines.where({ h }).and(uEqu).first()
		);
	}

	// read line if exists then update else add. Also update weak deps.
	static writeLine(h, u, v, t, id, lineWeakDeps) {
		const { db } = Cache;
		const { lines, weakDeps } = db;

		lineWeakDeps = lineWeakDeps.map(e => ({ lineId: id, ...e }));

		return db.transaction('rw', [lines, weakDeps], tx =>
			lines.put({ h, u, t, v }, id).then(
				lineId => weakDeps.where({ lineId }).primaryKeys()
			).then(
				deps => weakDeps.bulkDelete(deps)
			).then(
				() => weakDeps.bulkAdd(lineWeakDeps)
			)
		);
	}

	// invalidate previously written lines, by weak deps filtration.
	static invalidateLines(data, invWeakDeps) {
		const { db } = Cache;
		const { lines, weakDeps } = db;

		return db.transaction('rw', [lines, weakDeps], tx =>
			weakDeps.where(invWeakDeps).toArray().then(deps => {
				const lineIds = [...new Set(deps.map(({ lineId }) => lineId))];
				const depsIds = deps.map(({ id }) => id);
				return Dexie.Promise.all([
					lines.bulkDelete(lineIds),
					weakDeps.bulkDelete(depsIds)
				]);
			})
		);
	}
}
//------------------------------------------------------------------------------
(function () {
	if (DEVELOPMENT)
		Dexie.debug = true;

	const db = Cache.db = new Dexie('backend-cache');

	// Define schema
	db.version(1).stores({
		lines: '++id, h', // Primary Key is auto-incremented (id)
		weakDeps: '++id, lineId, [ type, key, value ]' // Primary Key is auto-incremented (id)
	});

	// Open the database
	//db.open().catch(e => console.error('Open database failed: ' + e));

	const h = 5557, u = '{r:r,a:true}', v = { cols: [], rows: [{ a: 1 }] };
	const weakDeps = [
		{ type: 'products', key: 'becf9099-35b9-11e9-823c-001e6749672d', value: undefined },
		{ type: 'products', key: '5552e77a-35b9-11e9-823c-001e6749672d', value: undefined },
		{ type: 'products', key: 'f9845c28-35b6-11e9-823c-001e6749672d', value: undefined }
	];

	// {
	// 	const map = new HashMap(weakDeps.map(
	// 		e => [[e.type, e.key, e.value], { lineId: 1, ...e }]
	// 	));
	// 	console.log(map.values());
	// 	map.delete([weakDeps[2].type, weakDeps[2].key, weakDeps[2].value]);
	// 	console.log(map.values());
	// 	map.delete([weakDeps[1].type, weakDeps[1].key, weakDeps[1].value]);
	// 	console.log(map.values());
	// 	map.delete([weakDeps[0].type, weakDeps[0].key, weakDeps[0].value]);
	// 	console.log(map.values());
	// }

	let beginLoad = (new Date()).getTime();

	for (let i = 1; i <= 2000; i++)
		weakDeps.push({ type: 'products', key: i, value: undefined });

	let endLoad = (new Date()).getTime();
	let ellapsedLoad = endLoad - beginLoad;
	console.log(`load: ${ellapsedLoad}ms`);

	const replaceWeakDeps = lineId => {
		return db.weakDeps.where({ lineId }).toArray().then(items => {
			beginLoad = (new Date()).getTime();
			const m = weakDeps.map(
				e => [[e.type, e.key, e.value], { lineId, ...e }]
			);
			endLoad = (new Date()).getTime();
			ellapsedLoad = endLoad - beginLoad;
			console.log(`map load: ${ellapsedLoad}ms`);
			beginLoad = (new Date()).getTime();
			const map = new HashMap(m);
			endLoad = (new Date()).getTime();
			ellapsedLoad = endLoad - beginLoad;
			console.log(`hash map load: ${ellapsedLoad}ms`);
			const delIds = [];

			for (const { id, type, key, value } of items) {
				const k = [type, key, value];

				if (map.has(k))
					// no need add exists deps
					map.delete(k);
				else
					// delete undefined deps
					delIds.push(id);
			}

			return [
				db.weakDeps.bulkDelete(delIds),
				db.weakDeps.bulkAdd(map.values())
			];
		});
	};

	db.transaction('rw', [db.lines, db.weakDeps], tx =>
		db.lines.where({ h }).and(line => line.u === u).first().then(line =>
			(line
				? db.lines.update(line.id, { v }).then(() => replaceWeakDeps(line.id))
				: db.lines.add({ h, u, v }).then(id => replaceWeakDeps(id))
			).then(bulk => Dexie.Promise.all(bulk))
		)
	).then(r => {
		console.log(r);
		return r;
	}).catch(e => {
		console.error(e.stack || e);
	});
})();
//------------------------------------------------------------------------------
