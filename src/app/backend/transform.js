//------------------------------------------------------------------------------
const digit0 = '0'.charCodeAt(0);
const digit9 = '9'.charCodeAt(0);
//------------------------------------------------------------------------------
export default function transform(raw) {
	if (Array.isArray(raw)) {
		let c = 0;

		for (let i = raw.length - 1; i >= 0; i--) {
			const { alias } = raw[i] = transform(raw[i]);

			if (alias) {
				raw[alias] = raw[i];
				c++;
			}
		}

		if (c === raw.length) {
			const r = {};

			for (const k of Object.keys(raw)) {
				c = k.charCodeAt(0);

				if (c < digit0 || c > digit9)
					r[k] = raw[k];
			}

			raw = r;
		}
	}
	else {
		const { cols, rows } = raw;

		if (cols && rows) {
			let i, j, k, v, l1, l2;

				for (l1 = rows.length, i = 0; i < l1; i++) {
					const now = {}, row = rows[i], { r, t } = row;

					for (l2 = r.length, j = 0; j < l2; j++) {
						k = cols[j];

						switch (t[j]) {
							case 0: // null
								now[k] = r[j] === 0 ? null : undefined;
								break;
							case 1: // string
								now[k] = r[j];
								break;
							case 2: // boolean
								now[k] = r[j] !== 0;
								break;
							case 3: // numeric
								now[k] = r[j];
								break;
							case 4: // date
								now[k] = new Date(Date.parse(r[j]));
								break;
							case 5: // link
								now[k] = r[j];
								break;
							case 6: // array
								now[k] = r[j];
								break;
							case 7: // structure - object
								v = r[j];
								now[k] = v.cols && (v.rows || v.grps)
									? transform(v) : v;
								break;
							default:
								throw new Error('Unsupported value type in row transformation');
						}
					}

					rows[i] = now;
				}
		}
		else {
			for (const k of Object.keys(raw)) {
				const v = raw[k];

				if (v.cols && v.rows)
					raw[k] = transform(v);
			}
		}
	}

	return raw;
}
//------------------------------------------------------------------------------
