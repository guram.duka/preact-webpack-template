//------------------------------------------------------------------------------
/*eslint comma-dangle:*/
//------------------------------------------------------------------------------
import webpack from 'webpack';
import yargs from 'yargs';
//------------------------------------------------------------------------------
const {
	optimizeMinimize
} = yargs.alias('p', 'optimize-minimize').argv;
const nodeEnv = optimizeMinimize ? 'production' : 'development';
//------------------------------------------------------------------------------
import htmlMinifier from 'html-minifier';
import CleanCSS from 'clean-css';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ProgressBarPlugin from 'progress-bar-webpack-plugin';
import WebpackPwaManifest from 'webpack-pwa-manifest';
import HtmlBeautifyPlugin from 'html-beautify-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CompressionPlugin from 'compression-webpack-plugin';
import WorkboxPlugin from 'workbox-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import DashboardPlugin from 'webpack-dashboard/plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import UglifyJSPlugin from 'uglifyjs-webpack-plugin';
import ScriptExtHtmlWebpackPlugin from 'script-ext-html-webpack-plugin';
import BundleAnalyzerPlugin from 'webpack-bundle-analyzer';
import TerserPlugin from 'terser-webpack-plugin';
import fs from 'fs';
import path from 'path';
import chalk from 'chalk';
import dotenv from 'dotenv';
//------------------------------------------------------------------------------
dotenv.config();
//------------------------------------------------------------------------------
const sourceMapInProd = false;
const compressionInProd = false;
const extractCSS = false;
//------------------------------------------------------------------------------
// defining the source path
const cwd = path.resolve(__dirname);
const src = path.resolve(cwd, 'src');
const nodeModules = path.resolve(cwd, 'node_modules');
const [
	publicPath,
	appTitle,
	appName,
	appShortName,
	appDescription,
	mode
] = (() => {
	const {
		env
	} = process;
	const a = [];

	a.push(env.PUBLIC_PATH || '/');
	a.push(env.TITLE || 'Progressive Web Application, preact webpack template');
	a.push(env.NAME || 'preact-webpack-template');
	a.push(env.SHORT_NAME || 'pwt');
	a.push(env.DESCRIPTION ||
		'My awesome Progressive Web Application, preact engine');
	a.push(env.NODE_ENV || 'development');

	return a;
})();
const isDev = mode === 'development';
const isProd = mode === 'production';
//------------------------------------------------------------------------------
// define file patterns and their loaders
const moduleRules = (() => {
	const ruleJSON = () => ({
		test: /\.json$/,
		loader: 'json-loader'
	});

	const ruleJS = () => ({
		test: /\.m?jsx?$/i,
		exclude: /(node_modules|bower_components)/,
		use: {
			loader: 'babel-loader',
			options: {
				presets: [
					'@babel/env',
					'@babel/react',
				],
				plugins: [
					//'@babel/plugin-proposal-object-rest-spread',
					'@babel/plugin-proposal-class-properties',
					'@babel/plugin-syntax-dynamic-import',
					'@babel/plugin-transform-runtime',
					[
						'@babel/plugin-transform-react-jsx', {
							pragma: 'preact.h'
						}
					]
				]
			}
		}
	});

	const ruleHTML = () => ({
		test: /\.(?:xml|html|txt|md)$/,
		use: {
			loader: 'html-loader',
			options: {
				attrs: [':data-src'],
				minimize: true,
				conservativeCollapse: false,
				removeAttributeQuotes: true
			}
		},
		exclude: [nodeModules, path.resolve(src, 'public/index.html'),
			/\.tpl\.(?:xml|html|txt|md)$/
		]
	});

	const ruleRawHTML = () => ({
		test: /\.tpl\.(?:xml|html|txt|md)$/,
		use: 'raw-loader',
		exclude: [nodeModules]
	});

	const ruleMEDIA = () => ({
		test: /\.(?:ico|png|gif|mp3|svg|webp|woff2?|ttf|eot|jpe?g|mkv|mp4|mov|ogg|webm)$/,
		use: {
			loader: isProd ? 'file-loader' : 'url-loader'
		}
	});

	const ruleCSS = (modules, exclude) => {
		let use = [{
			loader: 'css-loader',
			options: {
				modules,
				camelCase: true,
				//import: true,
				//importLoaders: 1,
				sourceMap: sourceMapInProd || isDev,
				...(extractCSS ? {localIdentName: '[local]__[hash:base64:5]'} : {})
			}
		},
		{
			loader: 'sass-loader',
			options: {
				sourceMap: sourceMapInProd || isDev,
				includePaths: [nodeModules],
				data: '$public-path: "' + publicPath + '" !default !global;'
			}
		}
		];

		if (isProd && extractCSS)
			use = ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use
			});

		if (isDev || !extractCSS)
			use = [
				{
					loader: 'style-loader',
					options: {
						sourceMap: sourceMapInProd || isDev
					}
				},
				...use
			];

		return {
			test: /\.s?[ac]ss$/,
			use,
			exclude
		};
	};

	return [
		ruleJSON(),
		ruleJS(),
		ruleHTML(),
		ruleRawHTML(),
		ruleMEDIA(),
		ruleCSS(false, [src]),
		ruleCSS(true, [nodeModules])
	];
})();
//------------------------------------------------------------------------------
const appLoaderHead = new CleanCSS({
	format: { wrapAt: 80 }
}).minify(
	fs.readFileSync(path.resolve(src, 'app/loader/style.css'), 'utf8')
).styles.replace(/\n/g, '\n\t\t\t\t');
//------------------------------------------------------------------------------
const appLoaderBody = htmlMinifier.minify(
	fs.readFileSync(path.resolve(src, 'app/loader/index.html'), 'utf8'), {
		collapseInlineTagWhitespace: true,
		conservativeCollapse: false,
		collapseWhitespace: true,
		maxLineLength: 80,
		minifyURLs: true,
		removeAttributeQuotes: true,
		removeComments: true,
		removeEmptyAttributes: true,
		removeEmptyElements: false,
		removeOptionalTags: true,
		removeRedundantAttributes: true,
		removeScriptTypeAttributes: true,
		removeStyleLinkTypeAttributes: true,
		removeTagWhitespace: true
	}
).replace(/ \n/g, '\n').replace(/\n/g, '\n\t\t\t');
//------------------------------------------------------------------------------
// Array of plugins to apply to build chunk
const plugins = (() => {
	const plugins = [
		new HtmlWebpackPlugin({
			title: appTitle,
			favicon: path.resolve(src, 'public/assets/icons/favicon.ico'),
			hash: false,
			template: path.resolve(src, 'public/index.html'),
			inject: 'body',
			loaderHead: appLoaderHead,
			loaderBody: appLoaderBody
		}),
		new webpack.DefinePlugin({ // plugin to define global constants
			// see in .eslintrc global excludes to avoid lint error is not defined
			PRODUCTION: JSON.stringify(isProd),
			DEVELOPMENT: JSON.stringify(isDev),
			PUBLIC_PATH: JSON.stringify(publicPath),
			API_KEY: JSON.stringify(process.env.API_KEY),
			VERSION: JSON.stringify('5fa3b9'),
			//'process.env.NODE_ENV': JSON.stringify(mode),
			'process.env': {
				'NODE_ENV': JSON.stringify(mode)
			}, // preact debug support
		}),
		// bundle.js will be executed only after DOM parsed, deferred
		/*new ScriptExtHtmlWebpackPlugin({
			//inline: 'loader',
			defaultAttribute: 'async'
		}),*/
		new webpack.ProvidePlugin({
			preact: 'preact'
		}),
		new HtmlBeautifyPlugin({
			config: {
				html: {
					end_with_newline: true,
					indent_size: 1,
					indent_with_tabs: true,
					indent_inner_html: true,
					preserve_newlines: false,
					extra_liners: '',
					'wrap-line-length': 80,
					unformatted: ['div', 'span']
				}
			},
			replace: [' type="text/javascript"']
		}),
		/*new ProgressBarPlugin({
			//format: '\u001b[90m\u001b[44mBuild\u001b[49m\u001b[39m [:bar] \u001b[32m\u001b[1m:percent\u001b[22m\u001b[39m (:elapsed) \u001b[2m:msg\u001b[22m',
			format: 'Build [:bar] ' + chalk.green.bold(':percent') + ' (:elapsed)',
			width: 40,
			renderThrottle: 200,
			summary: true,
			clear: true
		}),*/
		new WebpackPwaManifest({
			name: appName,
			short_name: appShortName,
			description: appDescription,
			background_color: '#ffffff',
			crossorigin: 'use-credentials', //can be null, use-credentials or anonymous
			start_url: '.',
			fingerprints: false,
			ios: true,
			icons: [{
				src: path.resolve(src, 'public/assets/icons/mstile-150x150.png'),
				destination: path.join('assets', 'icons'),
				size: 150,
			},
			{
				src: path.resolve(src,
					'public/assets/icons/android-chrome-192x192.png'),
				destination: path.join('assets', 'icons'),
				size: 192
			},
			{
				src: path.resolve(src,
					'public/assets/icons/android-chrome-512x512.png'),
				destination: path.join('assets', 'icons'),
				size: 512
			},
			{
				src: path.resolve(src, 'public/assets/icons/apple-touch-icon.png'),
				destination: path.join('assets', 'icons'),
				size: 180,
				ios: true
			}
			]
		}),
		new WorkboxPlugin.GenerateSW({
			// these options encourage the ServiceWorkers to get in there fast
			// and not allow any straggling 'old' SWs to hang around
			clientsClaim: true,
			skipWaiting: true
		})
	];

	if (isProd) {
		plugins.push(
			new CleanWebpackPlugin(['build'], {
				verbose: false
			}),
			new CopyWebpackPlugin(
				[{
					from: path.resolve(src, 'public'),
					ignore: '**/assets/icons/*'
				}], {
					//debug: 'debug'
				}
			),
			//new BundleAnalyzerPlugin.BundleAnalyzerPlugin({ analyzerMode: 'static' }),
		);

		if (extractCSS)
			plugins.push(
				new ExtractTextPlugin({
					filename: '[name].[id].[hash].css',
					allChunks: true
				})
			);

		if (compressionInProd)
			plugins.push(
				new CompressionPlugin({
					test: /\.(?:m?jsx?|css|ico|map)$/,
					algorithm: 'gzip',
					minRatio: 0.8,
					compressionOptions: {
						level: 9
					}
				}));
	}

	if (isDev)
		plugins.push(
			new webpack.HotModuleReplacementPlugin()
		);

	return plugins;
})();
//------------------------------------------------------------------------------
const entry = {
	 // webpack entry point. Module to start building dependency graph
	index: path.resolve(src, 'app/index.js')
};
//------------------------------------------------------------------------------
const output = {
	path: path.resolve(cwd, 'build'), // Folder to store generated bundle
	//filename: 'bundle.js', // Name of generated bundle after build
	publicPath, // public URL of the output directory when referenced in a browser
	pathinfo: true,
};
//------------------------------------------------------------------------------
if (isProd)
	output.filename = '[name].[id].[hash].js'; // code splitting and caching
//------------------------------------------------------------------------------
const performance = {
	hints: isProd && !sourceMapInProd ? 'warning' : false,
	maxAssetSize: 250000,
	maxEntrypointSize: 250000
};
//------------------------------------------------------------------------------
const optimization = {
	splitChunks: {
		chunks: 'async',
		minSize: 30000,
		maxSize: 0,
		minChunks: 1,
		maxAsyncRequests: 5,
		maxInitialRequests: 3,
		automaticNameDelimiter: '.',
		name: true,
		cacheGroups: {
			vendors: {
				test: /[\\/]node_modules[\\/]/,
				priority: -10
			},
			default: {
				minChunks: 2,
				priority: -20,
				reuseExistingChunk: true
			}
		}
	},
	minimizer: [
		new TerserPlugin({
			cache: true,
			parallel: true,
			sourceMap: sourceMapInProd, // Must be set to true if using source-maps in production
			//extractComments: false,
			terserOptions: {
				compress: !sourceMapInProd,
				output: {
					comments: sourceMapInProd,
					beautify: sourceMapInProd
				},
				keep_classnames: sourceMapInProd,
				keep_fnames: sourceMapInProd
			},
			/*terserOptions: {
				ecma: undefined,
				warnings: false,
				parse: {},
				compress: true,
				mangle: true, // Note `mangle.properties` is `false` by default.
				module: false,
				output: {
					shebang: true,
					comments: false,
					beautify: false,
					semicolons: false
				},
				toplevel: false,
				nameCache: null,
				ie8: false,
				keep_classnames: false,
				keep_fnames: false,
				safari10: false,
			}*/
		}),
	],
};
//------------------------------------------------------------------------------
const devServer = { // configuration for webpack-dev-server
	contentBase: path.resolve(src, 'public'), //source of static assets
	port: 7700, // port to run dev-server
	compress: true,
	hot: true
};
//------------------------------------------------------------------------------
const options = {
	mode,
	devtool: (sourceMapInProd && isProd) || isDev ? 'source-map' : undefined,
	watch: isDev, // Reload On File Change
	entry,
	output,
	module: {
		rules: moduleRules
	},
	plugins,
	performance,
	optimization,
	devServer,
	// https://webpack.js.org/configuration/stats/
	stats: isDev ? 'verbose' : {
		// fallback value for stats options when an option is not defined (has precedence over local webpack defaults)
		//all: undefined,

		// Add asset Information
		assets: false,

		// Sort assets by a field
		// You can reverse the sort with `!field`.
		// Some possible values: 'id' (default), 'name', 'size', 'chunks', 'failed', 'issuer'
		// For a complete list of fields see the bottom of the page
		assetsSort: '!size',

		// Add build date and time information
		builtAt: false,

		// Add information about cached (not built) modules
		cached: false,

		// Show cached assets (setting this to `false` only shows emitted files)
		cachedAssets: false,

		// Add children information
		children: false,

		// Add chunk information (setting this to `false` allows for a less verbose output)
		chunks: false,

		// Add namedChunkGroups information
		chunkGroups: false,

		// Add built modules information to chunk information
		chunkModules: false,

		// Add the origins of chunks and chunk merging info
		chunkOrigins: false,

		// Sort the chunks by a field
		// You can reverse the sort with `!field`. Default is `id`.
		// Some other possible values: 'name', 'size', 'chunks', 'failed', 'issuer'
		// For a complete list of fields see the bottom of the page
		chunksSort: '!size',

		// Context directory for request shortening
		//context: '../src/',

		// `webpack --colors` equivalent
		colors: true,

		// Display the distance from the entry point for each module
		depth: false,

		// Display the entry points with the corresponding bundles
		entrypoints: false,

		// Add --env information
		env: false,

		// Add errors
		errors: true,

		// Add details to errors (like resolving log)
		errorDetails: true,

		/*// Exclude assets from being displayed in stats
		// This can be done with a String, a RegExp, a Function getting the assets name
		// and returning a boolean or an Array of the above.
		excludeAssets: 'filter' | /filter/ | (assetName) => true | false | ['filter'] |
			[/filter/] | [(assetName) => true | false],

		// Exclude modules from being displayed in stats
		// This can be done with a String, a RegExp, a Function getting the modules source
		// and returning a boolean or an Array of the above.
		excludeModules: 'filter' | /filter/ | (moduleSource) => true | false | [
			'filter'
		] | [/filter/] | [(moduleSource) => true | false],

		// See excludeModules
		exclude: 'filter' | /filter/ | (moduleSource) => true | false | ['filter'] |
			[/filter/] | [(moduleSource) => true | false],
		*/
		// Add the hash of the compilation
		hash: true,

		// Set the maximum number of modules to be shown
		maxModules: 0,

		// Add built modules information
		modules: true,

		// Sort the modules by a field
		// You can reverse the sort with `!field`. Default is `id`.
		// Some other possible values: 'name', 'size', 'chunks', 'failed', 'issuer'
		// For a complete list of fields see the bottom of the page
		modulesSort: '!size',

		// Show dependencies and origin of warnings/errors (since webpack 2.5.0)
		moduleTrace: true,

		// Show performance hint when file size exceeds `performance.maxAssetSize`
		performance: true,

		// Show the exports of the modules
		providedExports: false,

		// Add public path information
		publicPath: true,

		// Add information about the reasons why modules are included
		reasons: true,

		// Add the source code of modules
		source: false,

		// Add timing information
		timings: true,

		// Show which exports of a module are used
		usedExports: false,

		// Add webpack version information
		version: true,

		// Add warnings
		warnings: true,

		/*// Filter warnings to be shown (since webpack 2.4.0),
		// can be a String, Regexp, a function getting the warning and returning a boolean
		// or an Array of a combination of the above. First match wins.
		warningsFilter: 'filter' | /filter/ | ['filter', /filter/] | (warning) =>
			true | false*/
	}
};
//------------------------------------------------------------------------------
if ((sourceMapInProd && isProd) || isDev)
	options.devtool = 'source-map';
//------------------------------------------------------------------------------
export default options;
//------------------------------------------------------------------------------
